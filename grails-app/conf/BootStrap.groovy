import org.codehaus.groovy.grails.commons.GrailsClass
import org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin

class BootStrap {

    def grailsApplication

    def init = { servletContext ->
        //domain validators are not automatically added to external POGOs that have been
        //registered as <GrailsDomainClass>es in resources.groovy.
        grailsApplication.domainClasses.each { GrailsClass gc ->
            DomainClassGrailsPlugin.addValidationMethods(grailsApplication,
                gc, grailsApplication.mainContext)
        }
    }
    def destroy = {
    }
}
