import org.codehaus.groovy.grails.commons.DomainClassArtefactHandler
import org.springframework.beans.factory.support.BeanDefinitionRegistry
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner
import org.springframework.core.type.filter.AnnotationTypeFilter
import grails.persistence.Entity

// Place your Spring DSL code here
beans = {
    def packages = ["net.biomodels.jummp.annotationstore", "net.biomodels.jummp.core.model",
            "net.biomodels.jummp.model", "net.biomodels.jummp.plugins.security"] as String[]
    BeanDefinitionRegistry simpleRegistry = new SimpleBeanDefinitionRegistry()
    ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(simpleRegistry, false)
    scanner.includeAnnotationConfig = false
    scanner.addIncludeFilter(new AnnotationTypeFilter(Entity.class))
    scanner.scan(packages)
    simpleRegistry.beanDefinitionNames?.each { String beanName ->
        BeanDefinition bean = simpleRegistry.getBeanDefinition(beanName)
        String beanClassName = bean.beanClassName
        application.addArtefact(DomainClassArtefactHandler.TYPE,
                                      Class.forName(beanClassName,
                                      true,
                                      Thread.currentThread().contextClassLoader))
    }
}
