package annotationstore.grails.client

class Foo {
    String bar
    static constraints = {
        bar blank: false
    }
}
