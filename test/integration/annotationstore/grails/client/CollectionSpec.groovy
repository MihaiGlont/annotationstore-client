package annotationstore.grails.client

import grails.test.spock.IntegrationSpec
import net.biomodels.jummp.model.Model
import net.biomodels.jummp.model.ModelFormat
import net.biomodels.jummp.model.Revision
import net.biomodels.jummp.plugins.security.Person
import net.biomodels.jummp.plugins.security.User


class CollectionSpec extends IntegrationSpec {

    /* @see org.grails.datastore.gorm.proxy.ProxyInstanceMetaClass */
    void "dynamic addTo method works as expected"() {
        when:
        Person p = new Person(userRealName: 'not me', institution: "M.E. PLC")

        then:
        p.save() != null
        !p.hasErrors()

        when:
        User self = new User(username: 'me', email: 'my@self.name', person: p,
            passwordExpired: false, password: 'obscure', accountExpired: false,
            accountLocked: false, enabled: true)

        then:
        self.save() != null
        !self.hasErrors()

        when:
        def sbmlL2V4 = new ModelFormat(name: "SBML", identifier: "SBML", formatVersion: "L2V4")

        then:
        sbmlL2V4.validate()
        sbmlL2V4.save() != null
        !sbmlL2V4.hasErrors()

        when:
        Model model = new Model(vcsIdentifier: "test/", submissionId: "MODEL00000400")
        def revision = new Revision(model: model, vcsId: "1", revisionNumber: 1,
            owner: self, minorRevision: false, name:"interesting model v1",
            uploadDate: new Date(), format: sbmlL2V4)

        then:
        revision.validate()

        when:
        model.addToRevisions(revision)

        then:
        model.save() != null
        !revision.hasErrors()
        !model.hasErrors()
    }
}
