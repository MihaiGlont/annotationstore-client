package annotationstore.grails.client

import grails.test.mixin.TestMixin
import grails.test.mixin.integration.IntegrationTestMixin
import net.biomodels.jummp.model.RepositoryFile
import net.biomodels.jummp.model.Revision
import spock.lang.*


@TestMixin(IntegrationTestMixin)
class ConstraintsSpec extends Specification {
    def applicationContext

    void "constraints are honoured for domain classes as well as externally defined POGOs"() {
        when:
        def rf = new RepositoryFile()
        def r = new Revision()
        def f = new Foo()

        then:
        f.validate() == false
        r.validate() == false
        rf.validate() == false

        when:
        f.bar = "something"
        rf.path = ".gitignore"
        rf.revision = r
        rf.description = "random file"

        then:
        f.validate() == true
        rf.validate() == true
        rf.errors.errorCount == 0
        rf.mimeType == "text/plain"
    }
}
